import axios from 'axios'
import {globalStore} from '@/globalStore'
import CryptoJS from 'crypto-js'

export const mergePatch = 'application/merge-patch+json; charset=UTF-8'
export const contentType = 'Content-Type'

export const httpConf = {
	baseURL: '',
	headers: {
		'Content-Type': 'application/json; charset=UTF-8',
	},
}

export var HTTP = axios.create(httpConf)
export var STATS = axios.create(httpConf)

const saveConf = function (address, appName) {
	globalStore.serverAddress = address
	globalStore.appName = appName
	httpConf.baseURL = address + 'app/' + appName
	HTTP = axios.create(httpConf)
	httpConf.baseURL = address + 'stats/' + appName
	STATS = axios.create(httpConf)
}

export { saveConf }

const calculateHash = function(payload, when) {
	//how to calculate hashes in JS:
	// let test = CryptoJS.SHA256('verysecret')
	// console.log(CryptoJS)
	// console.log(test.toString(CryptoJS.enc.Hex))
	// return test
	let stringified = ''
	if (payload !== '') {
		stringified = JSON.stringify(payload)
	}
	let hash = CryptoJS.HmacSHA256(stringified + when, globalStore.auth).toString(CryptoJS.enc.Hex)
	console.log(hash)
	return hash
}

const calculateLoginHash = function(password, when) {
	let hash = CryptoJS.HmacSHA256(when, password).toString(CryptoJS.enc.Hex)
	console.log(hash)
	return hash
}

export { calculateLoginHash }

export { calculateHash }

const getHTTPtime = function () {
	let now = new Date()
	return now.toUTCString()
}

const setWhen = function (config) {
	let when = getHTTPtime()
	console.log(when)
	config.headers['When'] = when
	return when
}

export { setWhen }
