import Vue from 'vue'
export const globalStore = new Vue({
	data: {
		title: 'Home',
		//these are default values, they are overwritten with values from localStorage when the App is mounted
		appName: '',
		serverAddress: '',
		auth: '',
		defaultSecret: '',
	}
})